﻿#include <iostream>
#include <fstream>
#include "DateTime.h"
#include "TimeInterval.h"

int main()
{
    try {
        DateTime dates[3];
        dates[0] = DateTime(2023, 1, 1, 12, 15);
        dates[1] = DateTime(2021, 8, 25, 14, 30);
        dates[2];

        std::cout << "Type the Date 3 (yyyy mm dd hh mm): ";
        std::cin >> dates[2];

        std::cout << "Date 1: " << dates[0] << '\n';
        std::cout << "Date 2: " << dates[1] << '\n';
        std::cout << "Date 3: " << dates[2] << '\n';
        std::cout << "-----------------------------------------------\n";

        std::string fileName = "file";
        std::fstream file;

        // Вывод в текстовый файл
        std::cout << "Writing to file " << fileName << ".txt ... ";
        file.open(fileName + ".txt", std::ios::out);
        if (!file) {
            throw std::string("can't open file");
        }
        file << dates[0] << dates[1] << dates[2];
        file.close();
        std::cout << "Success\n";

        // Ввод из текстового файла
        std::cout << "Reading from file " << fileName << ".txt ... ";
        file.open(fileName + ".txt", std::ios::in);
        if (!file) {
            throw std::string("can't open file");
        }
        file >> dates[0] >> dates[1] >> dates[2];
        file.close();
        std::cout << "Success\n";
        std::cout << "Date 1: " << dates[0] << '\n';
        std::cout << "Date 2: " << dates[1] << '\n';
        std::cout << "Date 3: " << dates[2] << '\n';
        std::cout << "-----------------------------------------------\n";

        // Вывод в бинарный файл
        std::fstream fileBin;
        std::cout << "Writing to file " << fileName << ".bin ... ";
        fileBin.open(fileName + ".bin", std::ios::out | std::ios::binary);
        fileBin.write((char*)&dates, 3);
        fileBin.write((char*)dates, 3 * sizeof(DateTime));
        fileBin.close();
        std::cout << "Success\n";

        // Ввод из бинарного файла
        std::cout << "Reading from file " << fileName << ".bin ... ";
        fileBin.open(fileName + ".bin", std::ios::in | std::ios::binary);
        fileBin.read((char*)&dates, 3);
        fileBin.read((char*)dates, 3 * sizeof(DateTime));
        fileBin.close();
        std::cout << "Success\n";
        std::cout << "Date 1: " << dates[0] << '\n';
        std::cout << "Date 2: " << dates[1] << '\n';
        std::cout << "Date 3: " << dates[2] << '\n';
    }
    catch (const std::string e) {
        std::cout << "[ERROR]: " << e << '\n';
        return -1;
    }

    return 0;
}